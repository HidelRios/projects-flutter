import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(title: "Contador de Pessoas", home: Home()));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _people = 0;
  int _limit = 10;
  final _vagas = 10;
  String _phrase = "Pode Entrar";

  void changePeople(int delta) {
    setState(() {
      _people += delta;
      _limit-=delta;

      if (_people < 0) {
        _people = 0;
        _limit = 10;
        _phrase = "Pode Entrar";
      } else if (_people < 10) {
        _phrase = "Pode Entrar";
      } else {
        _people = _vagas;
        _limit = 0;
        _phrase = "Lotado";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset("images/restaurant.jpg", fit: BoxFit.cover, height: 1000.0),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Pessoas: $_people",
              style: TextStyle(color: Colors.white),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: FlatButton(
                      onPressed: () {
                        changePeople(1);
                      },
                      child: Text("+1",
                          style:
                              TextStyle(fontSize: 40.0, color: Colors.white))),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: FlatButton(
                      onPressed: () {
                        changePeople(-1);
                      },
                      child: Text("-1",
                          style:
                              TextStyle(fontSize: 40.0, color: Colors.white))),
                )
              ],
            ),
            Text(
              _phrase,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                  fontStyle: FontStyle.italic),
            ),
            Text(
              "Vagas: $_limit",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                  fontStyle: FontStyle.italic),
            ),
          ],
        ),
      ],
    );
  }
}
